use std::{error::Error, fs, sync::Arc};

use inquire::InquireError;

fn main() -> Result<(), Box<dyn Error>> {
    let options = load_options()?;

    let task_choice = select_task(options.to_vec())?;
    println!("Executing task: {}", task_choice);

    let input_path = get_input_path()?;
    println!("Reading from file: {}", input_path);

    let input_content = get_input_content(&input_path)?;
    println!("Input content:\n{}", input_content);

    Ok(())
}

fn load_options() -> Result<Vec<Arc<String>>, Box<dyn Error>> {
    // This is a temporary setup until I figure out a way to dynamically load these
    // It's done to keep the function signature stable
    // And I am assuming here that the we won't know the options before runtime
    let options: Vec<Arc<String>> = vec!["Day 1:1"]
        .iter()
        .map(|&s| s.to_string())
        .map(|s| Arc::new(s))
        .collect();
    Ok(options)
}

fn select_task(options: Vec<Arc<String>>) -> Result<Arc<String>, InquireError> {
    inquire::Select::new("Which task do you want to run", options).prompt()
}

fn get_input_path() -> Result<String, InquireError> {
    let path = inquire::Text::new("Path to input file:").prompt()?;
    Ok(path)
}

fn get_input_content(file_path: &String) -> Result<String, Box<dyn Error>> {
    let content = fs::read_to_string(file_path)?;
    Ok(content)
}
