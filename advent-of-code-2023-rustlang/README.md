# [Advent of Code 2023](https://adventofcode.com/2023)


## Run program

1. Build files
  ```shell
  cargo build --release
  ```
2. Start program
  ```shell
  ./target/release/advent-of-code-2023
  ```
3. Select day
3. Give path to input file

## Snippets

### Rustlang Module System [Guide](https://www.sheshbabu.com/posts/rust-module-system/)

